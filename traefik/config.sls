{%- from slspath + '/map.jinja' import traefik with context -%}

traefik-config:
  file.managed:
    - name: {{ traefik.config_file }}
    - source: salt://{{ slspath }}/files/traefik.toml
    - template: jinja
    - context:
        config: {{ traefik.config|yaml() }}
    - user: {{ traefik.user}}
    - group: {{ traefik.group }}
    - mode: 0640
    - require:
      - user: traefik-user
    {%- if traefik.service %}
    - watch_in:
      - service: traefik
    {%- endif %}
