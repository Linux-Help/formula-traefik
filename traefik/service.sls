{%- from slspath+"/map.jinja" import traefik with context -%}

traefik-init-env:
  file.managed:
    {%- if grains['os_family'] == 'Debian' %}
    - name: /etc/default/traefik
    {%- else %}
    - name: /etc/sysconfig/traefik
    - makedirs: True
    {%- endif %}
    - user: root
    - group: root
    - mode: 0644
    - contents:
      - TRAEFIK_USER={{ traefik.user }}
      - TRAEFIK_GROUP={{ traefik.group }}
      - GOMAXPROCS=2
      - PATH=/usr/local/bin:/usr/bin:/bin

traefik-init-file:
  file.managed:
    {%- if salt['test.provider']('service') == 'systemd' %}
    - source: salt://{{ slspath }}/files/traefik.service
    - name: /etc/systemd/system/traefik.service
    - template: jinja
    - context:
        user: {{ traefik.user }}
        group: {{ traefik.group }}
        config_file: {{ traefik.config_file }}
    - mode: 0644
    {%- elif salt['test.provider']('service') == 'upstart' %}
    - source: salt://{{ slspath }}/files/traefik.upstart
    - name: /etc/init/traefik.conf
    - mode: 0644
    {%- else %}
    - source: salt://{{ slspath }}/files/traefik.sysvinit
    - name: /etc/init.d/traefik
    - mode: 0755
    {%- endif %}

{%- if traefik.service %}

traefik-service:
  service.running:
    - name: traefik
    - enable: True
    - watch:
      - file: traefik-init-env
      - file: traefik-init-file

{%- endif %}
