{%- from slspath + '/map.jinja' import traefik with context -%}

traefik-bin-dir:
  file.directory:
    - name: /usr/local/bin
    - makedirs: True

# Create traefik user
traefik-group:
  group.present:
    - name: {{ traefik.group }}

traefik-user:
  user.present:
    - name: {{ traefik.user }}
    - groups:
      - {{ traefik.group }}
    - home: /etc/traefik
    - createhome: False
    - system: True
    - require:
      - group: traefik-group

# Create directories
traefik-config-dir:
  file.directory:
    - name: /etc/traefik
    - user: {{ traefik.user }}
    - group: {{ traefik.group }}
    - mode: 0750

# Install agent
traefik-download:
  file.managed:
    - name: /tmp/traefik-{{ traefik.version }}
    - source: https://{{ traefik.download_host }}/containous/traefik/releases/download/v{{ traefik.version }}/traefik_linux-{{ traefik.arch }}
    - skip_verify: True
    - mode: 0755
    - unless: test -f /usr/local/bin/traefik-{{ traefik.version }}

traefik-install:
  file.rename:
    - name: /usr/local/bin/traefik-{{ traefik.version }}
    - source: /tmp/traefik-{{ traefik.version }}
    - require:
      - file: /usr/local/bin
    - watch:
      - file: traefik-download
    
traefik-clean:
  file.absent:
    - name: /tmp/traefik-{{ traefik.version }}
    - watch:
      - file: traefik-install

traefik-link:
  file.symlink:
    - target: traefik-{{ traefik.version }}
    - name: /usr/local/bin/traefik
    - watch:
      - file: traefik-install

traefik-setcap:
  cmd.run:
    - name: "setcap cap_net_bind_service=+ep /usr/local/bin/traefik-{{ traefik.version }}"
    - onchanges:
      - file: traefik-install
