======
traefik
======

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

Available states
================

.. contents::
    :local:

``traefik``
------------

Installs and configures the traefik service.

``traefik.install``
------------------

Downloads and installs the traefik binary file.

``traefik.config``
-----------------

Provision the traefik configuration files and sources.

``traefik.service``
------------------

Adds the traefik service startup configuration or script to an operating system.

To start a service during Salt run and enable it at boot time, you need to set following Pillar:

.. code:: yaml

    traefik:
      service: True

``traefik-template``
-------------------

Installs and configures traefik template.

.. vim: fenc=utf-8 spell spl=en cc=100 tw=99 fo=want sts=4 sw=4 et
